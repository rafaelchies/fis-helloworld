package br.redhat.consulting;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class HelloRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		restConfiguration()
			.port(8080)
			.component("servlet")
        		.contextPath("/rest/");

		rest("/hello")
			.produces("text/plain")
        		.get("/")
        		.route()
        		.to("direct:cbr")
        .endRest();
        
		from("direct:cbr").id("HelloRoute")
		.log("Hello World Message")
		.setBody(simple("FIS Hello World!"));
				
	}

}
